package org.beetl.sql.test;

import org.beetl.sql.core.annotatoin.*;
import org.beetl.sql.core.db.KeyHolder;
import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.mapper.BaseMapper;


/*
 *
 * gen by beetlsql mapper 2020-04-03
 */
public interface OrderDao extends BaseMapper<Order> {

}